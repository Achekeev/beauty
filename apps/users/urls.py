from django.urls import path
from apps.users.views import (
    RegistrationUserView,  UserPortfolioRetrieveUpdateDestroyView
)

urlpatterns = [
    path('registration/', RegistrationUserView.as_view(), name='Registration user'),
    path('user_portfolio/<int:pk>/', UserPortfolioRetrieveUpdateDestroyView.as_view(), name='User Portfolio'),
]
