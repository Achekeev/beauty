from django.dispatch import receiver
from django.db.models.signals import post_save
from django.db import models
from django.contrib.auth.models import AbstractUser

from apps.users.managers import CustomUserManager



class User(AbstractUser):
    email = models.EmailField(verbose_name='email address', unique=True)
    phone_number = models.CharField(verbose_name='phone number', max_length=15)  # uniqie =True
    balance = models.DecimalField(verbose_name='Balance', max_digits=6, decimal_places=2, default=0)
    username = models.CharField(verbose_name="username", max_length=31, null=True, blank=True)
    first_name = models.CharField(verbose_name="first name", max_length=31, blank=True, null=True)
    last_name = models.CharField(verbose_name="last name", max_length=31, blank=True, null=True)
    surname = models.CharField(verbose_name="surname", max_length=31, blank=True, null=True)
    is_active = models.BooleanField(default=False)


    objects = CustomUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    def __str__(self):
        return f'{self.email}'

