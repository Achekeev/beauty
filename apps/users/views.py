from rest_framework.generics import CreateAPIView, RetrieveUpdateDestroyAPIView

from apps.users.models import User
from apps.users.serializers import (
    RegistrationUserSerializer, UserPortfolioSerializer
)



class RegistrationUserView(CreateAPIView):
    queryset = User.objects.all()
    serializer_class = RegistrationUserSerializer


class UserPortfolioRetrieveUpdateDestroyView(RetrieveUpdateDestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserPortfolioSerializer
