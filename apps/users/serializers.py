from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from apps.users.models import User


class RegistrationUserSerializer(ModelSerializer):
    password = serializers.CharField(write_only=True)

    class Meta:
        model = User
        fields = ('id', 'email', 'password')

    def create(self, validated_data):
        pop_password = validated_data.pop('password')
        user = User.objects.create_user(password=pop_password, **validated_data)
        return user

        
class UserPortfolioSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'phone_number', 'username', 'first_name', 'last_name', 'surname', 'balance')
