from django.core.mail import send_mail

from celery import shared_task

from core.settings import env


@shared_task
def send_activation_code_to_email(instance):
    instance.generate_activation_code()
    message = f'Your activation code: {instance.activation_code}'
    send_mail("Activate account", message, 'onlineshop@gmail.com', [instance.email])
