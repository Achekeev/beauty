from django.shortcuts import get_object_or_404, redirect

from apps.users.models import User

from rest_framework import exceptions
from rest_framework.response import Response

from core.settings import env


class ActivateUserMixin:
    def get(self, request, activation_code):
        user = get_object_or_404(User, activation_code=activation_code)
        if user.is_active:
            raise exceptions.ParseError({'msg': "User already is active"})
        user.is_active = True
        user.activation_code = ''
        user.save()
        return Response({'msg': 'User is active'})
