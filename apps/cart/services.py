import random


def generate_random_number(instance):
    from apps.cart.models import Order
    try:
        number = random.randint(100000, 9999999999999999999999)
        order = Order.objects.filter(pk=instance.pk).update(number=str(number))
        return order
    except Exception:
        generate_random_number(instance)
