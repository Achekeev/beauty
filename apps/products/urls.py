from rest_framework.routers import DefaultRouter as DR
from apps.products.views import(
    CategoryViewSet, SubCategoryViewSet, SubSubCategoryViewSet, ProductViewSet, ImageViewSet
)
router = DR()

router.register('category', CategoryViewSet, basename='category')
router.register('subcategory', SubCategoryViewSet, basename='sub_category')
router.register('sub_subcategory', SubSubCategoryViewSet, basename='sub_sub_category')
router.register('image', ImageViewSet, basename='image')
router.register('product', ProductViewSet, basename='product')

urlpatterns = []

urlpatterns += router.urls