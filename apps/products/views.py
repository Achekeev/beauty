from rest_framework import viewsets
from apps.products.models import (
    Category, SubCategory, SubSubCategory, Product, ProductImage,
)
from apps.products.serializers import (
    CategorySerializer, SubCategorySerializer, SubSubCategorySerializer, ProductSerializer, ImageSerializer,
)

class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

class SubCategoryViewSet(viewsets.ModelViewSet):
    queryset = SubCategory.objects.all()
    serializer_class = SubCategorySerializer

class SubSubCategoryViewSet(viewsets.ModelViewSet):
    queryset = SubSubCategory.objects.all()
    serializer_class = SubSubCategorySerializer

class ImageViewSet(viewsets.ModelViewSet):
    queryset = ProductImage.objects.all()
    serializer_class = ImageSerializer

class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer



    