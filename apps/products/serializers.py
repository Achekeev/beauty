from rest_framework import serializers
from apps.products.models import (
    Category, SubCategory, SubSubCategory, Product, ProductImage
)


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'name', 'parent', )


class SubCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = SubCategory
        fields = ('id', 'name', 'category', )


class SubSubCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = SubSubCategory
        fields = ('id', 'name', 'sub_category', )

class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductImage
        fields = ('id', 'image', 'product', )


class ProductSerializer(serializers.ModelSerializer):
    images = ImageSerializer(read_only=True, many=True)

    class Meta:
        model = Product
        fields = (
            'id', 'name', 'sub_sub_category', 'available', 'price',
            'price_in_points', 'cashback', 'discount', 'video',
            'brend', 'made_in', 'volume', 'date_added', 'images',
        )

