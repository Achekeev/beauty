from rest_framework import serializers
from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=255, verbose_name='category_name')
    parent = models.ForeignKey('self', on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.name

class SubCategory(models.Model):
    name = models.CharField(max_length=255, verbose_name='sub_category_name')
    category = models.ForeignKey(Category, on_delete=models.CASCADE, verbose_name='category')

    def __str__(self):
        return self.name


class SubSubCategory(models.Model):
    name = models.CharField(max_length=255, verbose_name='sub_sub_category_name')
    sub_category = models.ForeignKey(SubCategory, on_delete=models.CASCADE, verbose_name='sub_category')

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=255, verbose_name='product_name')
    sub_sub_category = models.ForeignKey(SubSubCategory, on_delete=models.CASCADE, verbose_name='sub_sub_category')

    available = models.BooleanField(default=False, verbose_name='product_available')
    price = models.DecimalField(decimal_places=2, default=0, max_digits=20, verbose_name='product_price')
    price_in_points = models.DecimalField(decimal_places=2, default=0, max_digits=20, verbose_name='product_price_in_points')
    cashback = models.DecimalField(decimal_places=2, default=0, max_digits=20, verbose_name='product_cashback')
    discount = models.PositiveIntegerField(default=0, verbose_name='product_discount')
    video = models.FileField(verbose_name='product_video')
    brend = models.CharField(max_length=255, verbose_name='product_brend')
    made_in = models.CharField(max_length=255, verbose_name='product_made_in') 
    volume = models.CharField(max_length=255, verbose_name='product_volume')
    date_added = models.DateField(auto_now_add=True, verbose_name='product_date_added')

    @property
    def discount_price(self):
        if self.discount:
            return ((self.price * self.discount) / 100)
        return self.price

    def __str__(self):
        return self.name


class ProductImage(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='images')
    image = models.ImageField(verbose_name='images')
    
    def __str__(self):
        return self.product.name
        