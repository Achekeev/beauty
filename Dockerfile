FROM python:3

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /app

COPY requirements.txt /app/
ADD wsgi-entrypoint.sh /app/

RUN python -m pip install --upgrade pip
RUN pip install -r /app/requirements.txt

COPY . .
EXPOSE 8090

CMD [ "chmod", "+x", "wsgi-entrypoint.sh" ]

